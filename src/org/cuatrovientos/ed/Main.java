package org.cuatrovientos.ed;

import java.util.Scanner;

public class Main {
	
	public static void main(String args[]) {
		Scanner reader = new Scanner(System.in);
		String name;
		int option = 0, age;
		Scenary sc = new Scenary();

		do {
			sc.printMenu();
			option = Integer.parseInt(reader.nextLine());
			switch (option) {
			case 1:
				// Enter super hero
				System.out.println("Enter name: ");
				name = reader.nextLine(); 
				System.out.println("Enter age: ");
				age = Integer.parseInt(reader.nextLine()); 
				System.out.println("Enter goodPower: ");
				int goodPower = Integer.parseInt(reader.nextLine()); 
				System.out.println("Enter respect: ");
				int respect = Integer.parseInt(reader.nextLine()); 
			
				SuperHero mySuperHero = new SuperHero(name,age,goodPower,respect);
				sc.addCharacter(mySuperHero);
				break;
			case 2:
				// Enter villain
				System.out.println("Enter name: ");
				name = reader.nextLine(); 
				System.out.println("Enter age: ");
				age = Integer.parseInt(reader.nextLine()); 
				System.out.println("Enter evilPower: ");
				int evilPower = Integer.parseInt(reader.nextLine()); 
				System.out.println("Enter respect: ");
				int narcissism = Integer.parseInt(reader.nextLine()); 
			
				Villain myVillain = new Villain(name,age,evilPower,narcissism);
				sc.addCharacter(myVillain);
				break;
			case 3:
				// Show all fictional characters
				sc.showCharacters();
				break;
			case 4:
				// Remove fictional character
				System.out.println("Enter the index of the fictional character to remove");
				int index = Integer.parseInt(reader.nextLine());
				sc.removeCharacter(index);
				break;
			default:
				break;
			}
			
		}while(option != 0);
		
	}

}
