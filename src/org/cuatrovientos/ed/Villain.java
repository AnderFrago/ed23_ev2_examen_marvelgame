package org.cuatrovientos.ed;

public class Villain extends FictionalCharacter {
	
	int evilPower;
	int narcissism;
	
	public Villain(String name, int age) {
		super(name, age);
	}
	
	public Villain(String name, int age, int evilPower2, int narcissism2) {
		super(name, age);
		this.evilPower = evilPower2;
		this.narcissism = narcissism2;
	}
	
	@Override
	public double computeStrenght() {
		return evilPower * narcissism *  Math.random() * 0.9;
	}

	public int getEvilPower() {
		return evilPower;
	}

	public void setEvilPower(int evilPower) {
		this.evilPower = evilPower;
	}
	
	public String toString() {
		return super.getName() +" is a villain that has evil power=" +getEvilPower() +" and narcissism=" +this.narcissism;
	}

}
