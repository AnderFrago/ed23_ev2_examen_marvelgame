package org.cuatrovientos.ed;

public class SuperHero extends FictionalCharacter {
	
	int goodPower;
	int respect;

	public SuperHero(String name, int age) {
		super(name, age);
	}
	
	public SuperHero(String name, int age, int goodPower2, int respect2) {
		super(name, age);
		this.goodPower = goodPower2;
		this.respect = respect2;
	}

	@Override
	public double computeStrenght() {
		return goodPower * respect *  Math.random() ;
	}

	public int getGoodPower() {
		return goodPower;
	}

	public void setGoodPower(int evilgoodPowerPower) {
		this.goodPower = evilgoodPowerPower;
	}
	
	public String toString() {
		return super.getName() +" is a super hero that has good power=" +getGoodPower() +" and respect=" +this.respect;
	}

}
