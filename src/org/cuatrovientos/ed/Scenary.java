package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Scenary {
	String name;
	ArrayList<FictionalCharacter> listCharacters = new ArrayList<FictionalCharacter>();
	
	
	public void printMenu() {
		System.out.println("1. Enter Superhero");
		System.out.println("2. Enter Villain");
		System.out.println("3. Show all fictional characters");
		System.out.println("4. Remove fictional character");
		System.out.println("0. Finish");
	}

	public void addCharacter(FictionalCharacter fc) {
		listCharacters.add(fc);
	}
	
	public void removeCharacter(int index) {
		listCharacters.remove(index);
	}
	
	public void showCharacters() {
		for (FictionalCharacter fictionalCharacter : listCharacters) {
			System.out.println(fictionalCharacter.toString());
		}
	}

}
