package org.cuatrovientos.ed;

public abstract class FictionalCharacter {
	String name;
	int age;

	public FictionalCharacter() {
		this.name = "";
		this.age = 0;
	}

	public FictionalCharacter(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	
	public abstract double computeStrenght();
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
